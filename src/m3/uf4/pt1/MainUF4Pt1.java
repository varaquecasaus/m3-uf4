package m3.uf4.pt1;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class MainUF4Pt1 {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("CA", "ES"));

		/* EDIFICIS */

        Edifici Nostromo = new Edifici("c/ Joseph Conrad, 111", "contact@nostromo.com", 960555111);
        Edifici Fahrenheit = new Edifici("c/ Ray Bradbury, 451", "cronicas.marcianas@bradbury.com", 922555222);
        Edifici Arrakis = new Edifici("c/ Frank Herbert, 19-65", "atreides@dune.com", 978555333);

		/* ESPAIS */

        System.out.println(StringUtils.repeat("#", Espai.AMPLE_INFORME));
        System.out.println(StringUtils.center("ESPAIS", Espai.AMPLE_INFORME));
        System.out.println(StringUtils.repeat("#", Espai.AMPLE_INFORME) + System.lineSeparator());

        Espai robertHeinlein = new SalaReunions(Nostromo, 1, "Robert Heinlein", true, true);
        Espai larryNiven = new SalaReunions(Nostromo, 1, "Larry Niven", false, true);
        Espai arthurCClarke = new Auditori(Fahrenheit, 0, "Arthur C. Clarke", 250);
        Espai stanislawLem = new AulaInformatica(Arrakis, -1, "Stanislaw Lem", 25, true, true, "solaris", "s1961$%");
        Espai poulAnderson = new AulaInformatica(Arrakis, -2, "Poul Anderson", 15, false, true, "guardians", "$%time1960");

        System.out.println(robertHeinlein.capcaleraEspai() + System.lineSeparator());
        System.out.println(larryNiven.capcaleraEspai() + System.lineSeparator());
        System.out.println(stanislawLem.capcaleraEspai() + System.lineSeparator());

		/* CALENDARIS */

        System.out.println(StringUtils.repeat("#", Espai.AMPLE_INFORME));
        System.out.println(StringUtils.center("ESPAIS + CALENDARIS", Espai.AMPLE_INFORME));
        System.out.println(StringUtils.repeat("#", Espai.AMPLE_INFORME) + System.lineSeparator());

        arthurCClarke.afegirReserva(sdf.parse("28-09-2014"));
        arthurCClarke.afegirReserva(sdf.parse("02-10-2014"));
        arthurCClarke.afegirReserva(sdf.parse("11-10-2014"));
        arthurCClarke.afegirReserva(sdf.parse("21-10-2014"));
        arthurCClarke.afegirReserva(sdf.parse("01-11-2014"));

        poulAnderson.afegirReserva(sdf.parse("29-10-2014"));
        poulAnderson.afegirReserva(sdf.parse("03-11-2014"));
        poulAnderson.afegirReserva(sdf.parse("18-11-2014"));
        poulAnderson.afegirReserva(sdf.parse("23-11-2014"));
        poulAnderson.afegirReserva(sdf.parse("18-09-2014"));

        System.out.println(arthurCClarke.capcaleraEspai());
        System.out.println(arthurCClarke.calendariEspai(10, 2014) + System.lineSeparator());
        System.out.println(poulAnderson.capcaleraEspai());
        System.out.println(poulAnderson.calendariEspai(11, 2014) + System.lineSeparator());

		/* CONSULTES RESERVES */

        System.out.println(StringUtils.repeat("#", Espai.AMPLE_INFORME));
        System.out.println(StringUtils.center("CONSULTES", Espai.AMPLE_INFORME));
        System.out.println(StringUtils.repeat("#", Espai.AMPLE_INFORME) + System.lineSeparator());

        if (poulAnderson.consultarReserva(sdf.parse("29-10-2014")))
            System.out.println("1) L'espai " + poulAnderson.getNom() + " està reservat el dia 29-10-2014");
        if (!poulAnderson.consultarReserva(sdf.parse("30-10-2014")))
            System.out.println("2) L'espai " + poulAnderson.getNom() + " està disponible el dia 30-10-2014");
    }

}
