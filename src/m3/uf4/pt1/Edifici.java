package m3.uf4.pt1;

public class Edifici {
    private String adreca;
    private String mailContacte;
    private int telefon;

    /**
     * @param adreca
     * @param mailContacte
     * @param telefon
     */
    public Edifici(String adreca, String mailContacte, int telefon) {
        super();
        this.adreca = adreca;
        this.mailContacte = mailContacte;
        this.telefon = telefon;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getMailContacte() {
        return mailContacte;
    }

    public void setMailContacte(String mailContacte) {
        this.mailContacte = mailContacte;
    }

    public int getTelefon() {
        return telefon;
    }

    public void setTelefon(int telefon) {
        this.telefon = telefon;
    }


}
