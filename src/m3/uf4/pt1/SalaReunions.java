package m3.uf4.pt1;

public class SalaReunions extends Espai {
    private boolean aacc;
    private boolean wifi;

    /**
     * @param edifici
     * @param planta
     * @param nom
     * @param aacc
     * @param wifi
     */
    public SalaReunions(Edifici edifici, int planta, String nom,
                        boolean aacc, boolean wifi) {
        super(edifici, planta, nom);
        this.aacc = aacc;
        this.wifi = wifi;
    }


    public boolean getAacc() {
        return aacc;
    }


    public void setAacc(boolean aacc) {
        this.aacc = aacc;
    }


    public boolean getWifi() {
        return wifi;
    }


    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }


    @Override
    public String tipusEspai() {
        return "Sala de Reunions";
    }

    @Override
    public String[] infoEquipaments() {
        String[] equips = new String[2];
        equips[0] = "Aire acondicionat? ";

        if (this.getAacc()) equips[0] += "Si";
        else equips[0] += "No";

        equips[1] = "Accés Wifi? ";
        if (this.getWifi()) equips[1] += "Si";
        else equips[1] += "No";

        return equips;

    }
}
