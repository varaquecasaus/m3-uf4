package m3.uf4.pt1;

public class AulaInformatica extends Espai {

    private int equips;
    private boolean impressora;
    private boolean projector;
    private String user;
    private String password;

    public int getEquips() {
        return equips;
    }

    public void setEquips(int equips) {
        this.equips = equips;
    }

    public boolean isImpressora() {
        return impressora;
    }

    public void setImpressora(boolean impressora) {
        this.impressora = impressora;
    }

    public boolean isProjector() {
        return projector;
    }

    public void setProjector(boolean projector) {
        this.projector = projector;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param edifici
     * @param planta
     * @param nom
     * @param equips
     * @param impressora
     * @param projector
     * @param user
     * @param password
     */

    public AulaInformatica(Edifici edifici, int planta, String nom,
                           int equips, boolean impressora, boolean projector,
                           String user, String password) {
        super(edifici, planta, nom);
        this.equips = equips;
        this.impressora = impressora;
        this.projector = projector;
        this.user = user;
        this.password = password;
    }

    @Override
    public String tipusEspai() {
        return "Aula Informàtica";
    }

    @Override
    public String[] infoEquipaments() {
        String[] equipaments = new String[4];
        equipaments[0] = "L'aula disposa de " + this.getEquips() + " ordinadors.";
        equipaments[1] = "L'accés als equips és: " + user + "/" + password;

        if (impressora) equipaments[2] = "Hi ha impressora";
        else equipaments[2] = "No hi ha impressora";

        if (projector) equipaments[3] = "Hi ha projector";
        else equipaments[3] = "No hi ha projector";

        return equipaments;
    }


}
