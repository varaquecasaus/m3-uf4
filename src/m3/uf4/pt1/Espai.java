package m3.uf4.pt1;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class Espai implements Reservable {
    public static final int MIDA_RESERVES = 10;
    public static final int AMPLE_INFORME = 57;
    private static final int W_COL_1 = 3;
    private static final int W_COL_2 = 25;
    private static final int W_COL_3 = 20;
    private static final int W_COL_4 = 6;
    private static final int W_COL_EQUIP = 55;
    protected Edifici edifici;
    protected int planta;
    protected String nom;
    private Date[] reserves;
    private int next = 0;

    /**
     * @param edifici
     * @param planta
     * @param nom
     */
    public Espai(Edifici edifici, int planta, String nom) {
        super();
        this.reserves = new Date[MIDA_RESERVES];
        this.edifici = edifici;
        this.planta = planta;
        this.nom = nom;
    }

    public Edifici getEdifici() {
        return edifici;
    }

    public void setEdifici(Edifici edifici) {
        this.edifici = edifici;
    }

    public int getPlanta() {
        return planta;
    }

    public void setPlanta(int planta) {
        this.planta = planta;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public abstract String tipusEspai();

    public abstract String[] infoEquipaments();

    public String capcaleraEspai() {
        String[] datos = new String[6];
        String[] equips = this.infoEquipaments();
        Edifici edifici = this.getEdifici();
        datos[0] = edifici.getAdreca();
        datos[1] = this.tipusEspai();
        datos[2] = "Telf: " + edifici.getTelefon();
        datos[3] = this.getNom();
        datos[4] = edifici.getMailContacte();
        datos[5] = "" + this.getPlanta();
        /*
         *------------------------------------*
		 *		     GENERAR ESPAI            *
		 *------------------------------------*
		 */
        String capcalera = StringUtils.repeat('-', AMPLE_INFORME);
        capcalera += System.lineSeparator();
        capcalera += generarEspai(datos);

		/*
         *------------------------------------*
		 *		  GENERAR EQUIPAMENTS         *
		 *------------------------------------*
		 */
        capcalera += System.lineSeparator();
        capcalera += StringUtils.rightPad("|", 1);
        capcalera += StringUtils.center("EQUIPAMENTS", W_COL_EQUIP);
        capcalera += StringUtils.rightPad("|", 1);
        capcalera += System.lineSeparator();
        capcalera += StringUtils.repeat('-', AMPLE_INFORME);
        capcalera += generarEquipaments(equips);

        return capcalera;
    }

    private String generarEspai(String[] data) {
        String header = "";
        for (int i = 0; i < data.length; i = i + 2) {
            header += StringUtils.rightPad("|", W_COL_1);
            header += StringUtils.rightPad(data[i], W_COL_2);
            header += StringUtils.rightPad("|", W_COL_1);
            header += StringUtils.leftPad(data[i + 1], W_COL_3);
            header += StringUtils.leftPad("|", W_COL_4);
            header += System.lineSeparator();
        }
        header += StringUtils.repeat('-', AMPLE_INFORME);
        return header;
    }

    private String generarEquipaments(String[] equips) {
        String header = "";
        for (String equip : equips) {
            header += System.lineSeparator();
            header += StringUtils.rightPad("|", 1);
            header += StringUtils.center(equip, W_COL_EQUIP);
            header += StringUtils.rightPad("|", 1);
        }
        header += System.lineSeparator();
        header += StringUtils.repeat('-', AMPLE_INFORME);

        return header;
    }

    public String calendariEspai(int mes, int any) throws ParseException {
        String infoCalendari = StringUtils.rightPad("|", 1);
        Calendar calendari = Calendar.getInstance();
        calendari.set(any, mes - 1, 1);
        infoCalendari += generateHeaderCal(calendari);
        infoCalendari += System.lineSeparator() + generateCalendar(calendari);
        return infoCalendari;
    }

    private String generateHeaderCal(Calendar calendari) {
        String capCalendari = "";
        capCalendari += StringUtils.center("CALENADARI DE RESERVES", W_COL_EQUIP);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += System.lineSeparator();
        capCalendari += StringUtils.rightPad("|", 1);
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM", new Locale("CA", "ES"));
        capCalendari += StringUtils.center(sdf.format(calendari.getTime()) + " del " + calendari.get(Calendar.YEAR), W_COL_EQUIP);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += System.lineSeparator();
        capCalendari += StringUtils.repeat('-', AMPLE_INFORME);
        capCalendari += System.lineSeparator();
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DL.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DT.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DC.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DJ.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DV.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DS.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += StringUtils.center("DG.", 7);
        capCalendari += StringUtils.rightPad("|", 1);
        capCalendari += System.lineSeparator();
        capCalendari += StringUtils.repeat('-', AMPLE_INFORME);

        return capCalendari;
    }

    private String generateCalendar(Calendar calendari) throws ParseException {
        String cosCalendari = "";
        int[] dades = new int[5];

        dades[0] = calendari.get(Calendar.DAY_OF_MONTH);
        dades[1] = calendari.getActualMaximum(Calendar.DAY_OF_MONTH);
        dades[2] = calendari.get(Calendar.DAY_OF_WEEK) - 1;
        dades[3] = calendari.get(Calendar.MONTH) + 1;
        dades[4] = calendari.get(Calendar.YEAR);
        if (dades[2] == 0) dades[2] = 7;

        for (int i = 1; i < dades[2]; i++) {
            cosCalendari += StringUtils.rightPad("|", 1);
            cosCalendari += StringUtils.center("-", 7);
        }
        while (dades[0] <= dades[1]) {

            SimpleDateFormat sdf = new SimpleDateFormat("d-MM-yyyy", new Locale("CA", "ES"));
            Date fecha = sdf.parse(dades[0] + "-" + dades[3] + "-" + dades[4]);
            if (this.consultarReserva(fecha)) {
                cosCalendari += StringUtils.rightPad("|", 1);
                cosCalendari += StringUtils.center("XX", 7);
            } else {
                cosCalendari += StringUtils.rightPad("|", 1);
                cosCalendari += StringUtils.center("" + dades[0], 7);
            }
            if (dades[2] == 7) {
                cosCalendari += StringUtils.rightPad("|", 1);
                cosCalendari += System.lineSeparator();
                dades[2] = 0;
            }
            dades[2]++;
            dades[0]++;
        }
        for (int i = dades[2]; i <= 7; i++) {
            cosCalendari += StringUtils.rightPad("|", 1);
            cosCalendari += StringUtils.center("-", 7);
        }
        cosCalendari += StringUtils.rightPad("|", 1);
        cosCalendari += System.lineSeparator();
        return cosCalendari;
    }

    @Override
    public void afegirReserva(Date dia) {
        if (next < reserves.length) {
            reserves[next] = dia;
            next++;
        }
    }

    @Override
    public boolean consultarReserva(Date dia) {
        for (Date reserva : reserves) {
            if (reserva != null && reserva.equals(dia)) {
                return true;
            }
        }
        return false;
    }


}
