package m3.uf4.pt1;

import java.util.Date;

public interface Reservable {

    void afegirReserva(Date dia);

    boolean consultarReserva(Date dia);
}
