package m3.uf4.pt1;

public class Auditori extends Espai {
    private int capacitat;

    /**
     * @param edifici
     * @param planta
     * @param nom
     * @param capacitat
     */
    public Auditori(Edifici edifici, int planta, String nom, int capacitat) {
        super(edifici, planta, nom);
        this.capacitat = capacitat;
    }

    public int getCapacitat() {
        return capacitat;
    }

    public void setCapacitat(int capacitat) {
        this.capacitat = capacitat;
    }

    @Override
    public String tipusEspai() {

        return "Auditori";
    }

    @Override
    public String[] infoEquipaments() {
        String[] equips = new String[1];

        equips[0] = "L'aforament de l'auditori és de " + capacitat + " persones";

        return equips;
    }
}
