# README #

Este es el README de la UF4 de M3.

### Que es esto? ###

El codigo del repositorio esta generado en intelliJ de JetBrains pero supongo que copiando la carpeta src 
todas las librerias (y "activandolas" en el IDE que uses) a tu proyecto puedes hacer "funcionar" todo el proyecto.

### Como me lo descargo? ###

Si tienes intelliJ solo tienes que importar el proyecto desde el control de versiones del mismo IDE, si tienes otro,
desconozco si tambien utilizando el control de versiones de el IDE en cuestion no haya problemas, en cualquier caso siempre
puedes descargar el proyecto desde la pestana downloads y copiar las carpetas que el proyecto necesite al tuyo.
